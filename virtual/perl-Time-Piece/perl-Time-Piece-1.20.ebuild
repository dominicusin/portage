# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/virtual/perl-Time-Piece/perl-Time-Piece-1.20.ebuild,v 1.2 2011/04/24 15:28:59 grobian Exp $

DESCRIPTION="Object Oriented time objects"
HOMEPAGE=""
SRC_URI=""

LICENSE=""
SLOT="0"
KEYWORDS="~alpha ~amd64 ~ia64 ~ppc ~ppc64 ~sparc ~x86 ~sparc-fbsd ~x86-solaris"
IUSE=""

RDEPEND="~perl-core/Time-Piece-${PV}"
