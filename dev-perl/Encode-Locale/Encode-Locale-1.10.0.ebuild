# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-perl/Encode-Locale/Encode-Locale-1.10.0.ebuild,v 1.3 2011/05/15 17:45:13 armin76 Exp $

EAPI=3

MODULE_AUTHOR=GAAS
MODULE_VERSION=1.01
inherit perl-module

DESCRIPTION="Determine the locale encoding"

SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~ia64 ~m68k ~s390 ~sh ~sparc ~x86"
IUSE=""

SRC_TEST=do
