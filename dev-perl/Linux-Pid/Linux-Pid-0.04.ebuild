# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-perl/Linux-Pid/Linux-Pid-0.04.ebuild,v 1.2 2011/05/15 17:22:02 armin76 Exp $

EAPI="3"

MODULE_AUTHOR="RGARCIA"

inherit perl-module

DESCRIPTION="Interface to Linux getpp?id functions"

SRC_TEST="do"

LICENSE="|| ( Artistic GPL-1 GPL-2 GPL-3 )"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~ia64 ~sparc ~x86"
IUSE=""
