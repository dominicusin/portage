# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-perl/Math-Round/Math-Round-0.06.ebuild,v 1.6 2011/06/05 20:11:55 jer Exp $

MODULE_AUTHOR=GROMMEL
inherit perl-module

DESCRIPTION="Perl extension for rounding numbers"
LICENSE="|| ( Artistic GPL-2 )"

SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86 ~x86-freebsd ~amd64-linux ~x86-linux ~ppc-macos ~x64-macos ~x86-macos ~x86-solaris"

IUSE=""
DEPEND=""
