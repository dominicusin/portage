# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-perl/Devel-Size/Devel-Size-0.760.0.ebuild,v 1.1 2011/05/12 15:49:50 tove Exp $

EAPI=4

MODULE_AUTHOR=NWCLARK
MODULE_VERSION=0.76
inherit perl-module

DESCRIPTION="Perl extension for finding the memory usage of Perl variables"

SLOT="0"
KEYWORDS="~amd64 ~ia64 ~ppc64 ~sparc ~x86"
IUSE=""

SRC_TEST="do"
