# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-perl/MIME-Charset/MIME-Charset-1.8.2.ebuild,v 1.1 2011/05/28 19:21:03 tove Exp $

EAPI=4

MODULE_AUTHOR=NEZUMI
MODULE_VERSION=1.008.2
inherit perl-module

DESCRIPTION="Charset Informations for MIME"

SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

SRC_TEST=do
