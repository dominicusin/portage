# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/app-xemacs/slider/slider-1.16.ebuild,v 1.4 2011/06/28 21:43:17 ranger Exp $

SLOT="0"
IUSE=""
DESCRIPTION="User interface tool."
PKG_CAT="standard"

KEYWORDS="alpha ~amd64 ppc ~ppc64 sparc x86"

inherit xemacs-packages
