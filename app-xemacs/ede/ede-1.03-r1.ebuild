# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/app-xemacs/ede/ede-1.03-r1.ebuild,v 1.5 2011/06/28 21:43:17 ranger Exp $

PKG_CAT="standard"

inherit xemacs-packages

SLOT="0"
IUSE=""
DESCRIPTION="Emacs Development Environment."

KEYWORDS="alpha ~amd64 ppc sparc x86"
