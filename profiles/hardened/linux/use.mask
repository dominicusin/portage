# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/profiles/hardened/linux/use.mask,v 1.11 2011/06/30 19:15:46 blueness Exp $

-hardened

emul-linux-x86

# tcc is x86-only
tcc

# precompiled headers are not compat with ASLR.
pch

# prelink is masked for hardened
prelink

# opencl is used by nvidia drivers, bug #367225
opencl
