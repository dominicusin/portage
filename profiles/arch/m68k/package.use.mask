# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/profiles/arch/m68k/package.use.mask,v 1.7 2011/05/09 14:27:48 ssuominen Exp $

# Mike Frysinger <vapier@gentoo.org> (10 Feb 2011)
# Waiting for keywording #354309
net-firewall/iptables netlink

# Diego Pettenò <flameeyes@gentoo.org> (10 Nov 2007)
# Tests for Linux-PAM 0.99 require >=sys-libs/glibc-2.4
>=sys-libs/pam-0.99.8 test

# Masking since this pulls in app-crypt/pinentry, which isn't keyworded
dev-libs/opensc nsplugin

# Masking these to keep repoman happy
media-gfx/graphviz perl
