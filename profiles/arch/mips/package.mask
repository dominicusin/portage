# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/profiles/arch/mips/package.mask,v 1.12 2011/06/14 04:25:05 mattst88 Exp $

# Mask pam_ldap and nss_ldap - openldap dep missing.
sys-auth/pam_ldap
sys-auth/nss_ldap
