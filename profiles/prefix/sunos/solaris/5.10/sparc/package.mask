# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/profiles/prefix/sunos/solaris/5.10/sparc/package.mask,v 1.3 2011/04/06 17:34:50 grobian Exp $

# Fabian Groffen <grobian@gentoo.org> (28 Dec 2009)
# Fails to compile on Sparc (both 32-bits and 64-bits)
# http://bugs.gentoo.org/show_bug.cgi?id=293106
# http://bugs.gentoo.org/show_bug.cgi?id=332445
>=sys-devel/gcc-4.4.0
