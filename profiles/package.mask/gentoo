####################################################################
# $Header: /var/cvsroot/gentoo-x86/profiles/package.mask,v 1.12876 2011/06/30 15:39:25 dagger Exp $
#
# When you add an entry to the top of this file, add your name, the date, and
# an explanation of why something is getting masked. Please be extremely
# careful not to commit atoms that are not valid, as it can cause large-scale
# breakage, especially if it ends up in the daily snapshot.
#
## Example:
##
## # Dev E. Loper <developer@gentoo.org> (28 Jun 2012)
## # Masking  these versions until we can get the
## # v4l stuff to work properly again
## =media-video/mplayer-0.90_pre5
## =media-video/mplayer-0.90_pre5-r1
#
# - Best last rites (removal) practices -
# Include the following info:
# a) reason for masking
# b) bug # for the removal (and yes you should have one)
# c) date of removal (either the date or "in x days")
# d) the word "removal"
#
## Example:
##
## Dev E. Loper <developer@gentoo.org> (25 Jan 2012)
## Masked for removal in 30 days.  Doesn't work
## with new libfoo. Upstream dead, gtk-1, smells
## funny. (bug #987654)
## app-misc/some-package

#--- END OF EXAMPLES ---

# Robert Piasek <dagger@gentoo.org> (30 Jun 2011)
# Masking for testing
=net-fs/samba-3.6.0_rc2

# Justin Lecher <jlec@gentoo.org> (29 Jun 2011)
# ABI/API break
# Still don't know how to fix that.
>=sci-libs/mmdb-1.23
>=sci-chemistry/coot-0.6.2

# Pacho Ramos <pacho@gentoo.org> (29 Jun 2011)
# Mask for removal in 30 days x11-libs/gtksourceview:1.0
# and reverse dependencies, bug #354241
=x11-libs/gtksourceview-1.8.5-r1
app-editors/conglomerate
app-editors/mlview
app-editors/screem
dev-python/gtksourceview-python
app-editors/scribes
=dev-ruby/ruby-gtksourceview-0.19.4
=sci-electronics/oregano-0.69.0*

# Justin Lecher <jlec@gentoo.org> (27 Jun 2011)
# Only avalable version isn't in the tree.
# Mask until it is bumped
sci-chemistry/webmo

# Alexis Ballier <aballier@gentoo.org> (26 Jun 2011)
# Mask release candidate of ocaml
>=dev-lang/ocaml-3.12.1_rc
>=dev-ml/ocamlduce-3.12.0.0-r10

# Hans de Graaff <graaff@gentoo.org> (25 Jun 2011)
# Mask for testing, still incomplete and too central to
# all things ruby to just throw out there.
=dev-ruby/rubygems-1.8*

# Alexis Ballier <aballier@gentoo.org> (23 Jun 2011)
# Breaks API/ABI and has the same features as 0.7.1
>=media-video/ffmpeg-0.8

# Eray Aslan <eras@gentoo.org> (21 Jun 2011)
# Mask prerelease software
=app-crypt/heimdal-1.5_pre*

# Peter Volkov <pva@gentoo.org> (18 Jun 2011)
# Mask release candidate
=dev-util/oprofile-0.9.7_rc*

# Andreas K. Huettel <dilfridge@gentoo.org> (18 Jun 2011)
# Mask cups-1.5, too new and still needs a lot of testing
>=net-print/cups-1.4.9999

# Jorge Manuel B. S. Vicetto <jmbsvicetto@gentoo.org> (18 Jun 2011)
# Add mariadb mask to Robin's earlier mask
# Robin H. Johnson <robbat2@gentoo.org> (01 Feb 2010)
# Mask 5.4 series that is nearly a release candidate upstream (expected within the next 6 months)
# Mask 5.5/6.0 series that are alpha and and crazy experimental.
# Mask 5.2 virtual that exists in the overlay as well (it is MariaDB only)
=dev-db/mysql-5.4*
=virtual/mysql-5.4*
>=dev-db/mysql-5.5
>=virtual/mysql-5.5
=dev-db/mariadb-5.2*
=virtual/mysql-5.2*

# Eray Aslan <eras@gentoo.org> (18 Jun 2011)
# Dead upstream.  Lots of alternatives including mailx,
# nail, ssmtp, postfix...
# Removal in 30 days
mail-client/smtpclient

# Eray Aslan <eras@gentoo.org> (18 Jun 2011)
# No upstream. Does not work correctly. Bugs #275764 #370171.
# Several working alternatives, including dovecot, cyrus, mailutils.
# Removal in 30 days
net-mail/teapop

# Eray Aslan <eras@gentoo.org> (18 Jun 2011)
# Mask release candidate
=mail-mta/postfix-2.8.4_rc*

# Torsten Veller <tove@gentoo.org> (18 Jun 2011)
# Mask perl-5.14. See tracker bug #356171
=dev-lang/perl-5.14*

# Stuart Longland <redhatter@gentoo.org> (17 Jun 2011)
# Masked for removal within 30 days.  Will be replacing it with updated
# ebuilds to address numerous issues.  See bugs #336993, #336199, #369881
# and #335307.
=media-radio/svxlink-090426

# Stuart Longland <redhatter@gentoo.org> (17 Jun 2011)
# Masked for removal within 30 days.  There is a newer version upstream but it
# doesn't compile for me, and upstream is now dead.  As an alternative, have a
# look at media-radio/fldigi instead. (See bug #259451)
media-radio/gmfsk

# Pawel Hajdan jr <phajdan.jr@gentoo.org> (17 Jun 2011)
# Mask v8 versions used for www-client/chromium dev channel releases.
# Currently v8-3.4 versions are used for chromium-14.x versions.
>=dev-lang/v8-3.4

# Pawel Hajdan jr <phajdan.jr@gentoo.org> (17 Jun 2011)
# Dev channel releases are only for people who are developers or want more
# experimental features and accept a more unstable release.
>=www-client/chromium-14

# Samuli Suominen <ssuominen@gentoo.org> (15 Jun 2011)
# Retire gpixpod for bugs 221277 and 351307.   Removal in 30 days.
app-pda/gpixpod

# Eray Aslan <eras@gentoo.org> (15 Jun 2011)
# Mask experimental software
=mail-mta/postfix-2.9*

# Eray Aslan <eras@gentoo.org> (14 Jun 2011)
# Last release in 2000. Possible buffer overflow - bug #349786.
# Alternative net-mail/signify but better still most mail
# clients provide this service now.
# Removal in 30 days
net-mail/signature

# Eray Aslan <eras@gentoo.org> (14 Jun 2011)
# Dead upstream. Does not work properly - bug #179497 #370145.
# Several good alternatives, including dovecot, cyrus, mailutils.
# Removal in 30 days
net-mail/vm-pop3d

# Nirbheek Chauhan <nirbheek@gentoo.org> (14 Jun 2011)
# GNOME 3.1.x pre-releases for libraries in the tree
>=dev-libs/gobject-introspection-1.29
>=dev-libs/gjs-1.29
=media-libs/clutter-1.7*

# Christian Faulhammer <fauli@gentoo.org> (13 Jun 2011)
# Will update the database, give it one or two minor versions to
# settle the migration code
>=app-misc/gramps-3.3.0

# Michal Januszewski <spock@gentoo.org> (12 Jun 2011)
# Masked for removal in 30 days. The profiler is now provided by
# nvidia-cuda-toolkit.
dev-util/nvidia-cuda-profiler

# Sergei Trofimovich <slyfox@gentoo.org> (11 Jun 2011)
# Masked for removal in 30 days. 'Abandoned' by upstream.
# Fails to build on modern toolchain:
#    #284216, #367191, #267974
#
# Upstream now provides splitted packages for various gtk
# subsystems:
#     dev-haskell/pango
#     dev-haskell/cairo
#     dev-haskell/glib
#     dev-haskell/cairo
#     dev-haskell/glade
#     dev-haskell/gtk
dev-haskell/gtk2hs

# Diego E. Pettenò <flameeyes@gentoo.org> (09 Jun 2011)
#  on behalf of QA team
#
# Fails to build with GCC 4.6; replaced by net-misc/gogoc
# (same codebase, renamed and reversioned) which is somewhat
# maintained.
#
# Removal on 2011-07-09
net-misc/gateway6

# Diego E. Pettenò <flameeyes@gentoo.org> (8 Jun 2011)
# Broken upstream
=net-libs/libtirpc-0.2.2

# Sebastian Pipping <sping@gentoo.org> (7 Jun 2011)
# Experimental bump (bug #340425)
~sys-block/open-iscsi-2.0.872

# Bernard Cafarelli <voyageur@gentoo.org> (6 Jun 2011)
# Support for libobjc2, for adventurous people only for now
=gnustep-base/gnustep-make-2.6.1-r10

# Chí-Thanh Christopher Nguyễn <chithanh@gentoo.org> (6 Jun 2011)
# Mask prerelease of xorg-server 1.11
>=x11-base/xorg-server-1.10.99

# Jeroen Roovers <jer@gentoo.org> (5 Jun 2011)
# Masked until bug #370163 is resolved.
>=sys-block/parted-3.0

# Eray Aslan <eras@gentoo.org> (4 Jun 2011)
# Dead upstream. Not needed as Google supports
# IMAP access to mailboxes now. Bug #151470
# Removal in 30 days
net-mail/gml

# Tim Harder <radhermit@gentoo.org> (3 Jun 2011)
# Mask release candidates
=app-office/scribus-1.4.0_rc*

# Markos Chandras <hwoarang@gentoo.org> (3 Jun 2011)
# Dead upstrea, many open bugs, partially working with 
# kernel-2.6
# Bugs #159741, #159838, #165120, #165200, #231459,
# #273902, #278949, #305155, #330523, #369293
# Masked for removal in 30 days
# Alternative: sys-fs/lvm2
sys-fs/evms

# Andreas K. Huettel <dilfridge@gentoo.org> (3 Jun 2011)
# Requires KDE 4.4, which is not in the portage tree anymore.
# Please unmerge before upgrading KDE, and emerge afterwards
# net-wireless/bluedevil for bluetooth support in KDE 4.6.
# Masked for removal in 15 days
net-wireless/kbluetooth

# Markos Chandras <hwoarang@gentoo.org> (30 May 2011)
# Broken ebuild. Bug #351697
# - uses symlinked automake files;
# - doesn't provide autotools files to regen;
# - does not respect jobserver
# Masked for removal in 30 days
media-sound/spiralmodular

# Markos Chandras <hwoarang@gentoo.org> (30 May 2011)
# Does not build against gcc-4.5.2. Requires obsolete
# net-libs/rb_libtorrent-0.14.* which does not build
# against python-2.7. Alternatives: net-p2p/rtorrent
# Masked for removal in 30 days
net-p2p/hrktorrent

# Markos Chandras <hwoarang@gentoo.org> (28 May 2011)
# emesene-2 is on early stage of development and very 
# unstable
=net-im/emesene-2*

# Markos Chandras <hwoarang@gentoo.org> (26 May 2011)
# Masked for testng . Bug #347393
=net-analyzer/ntop-4.0.3

# Markos Chandras <hwoarang@gentoo.org> (26 May 2011)
# Masked for testing. Bug #308449
=net-dialup/freeradius-2.1.10

# Michael Sterrett <mr_bones_@gentoo.org> (25 May 2011)
# Needs an old version of bison that isn't in the tree anymore.
dev-lang/open64

# Jeroen Roovers <jer@gentoo.org> (24 May 2011)
# Opera Next is forever unstable and unsupported.
# <http://my.opera.com/desktopteam/blog>
www-client/opera-next

# Peter Volkov <pva@gentoo.org> (24 May 2011)
# Mask release candidates
=net-analyzer/wireshark-1.6.0_rc*

# Eray Aslan <eras@gentoo.org> (22 May 2011)
# Mask release candidates
=mail-filter/amavisd-new-2.7.0_rc*

# Arfrever Frehtes Taifersar Arahesis <arfrever@gentoo.org> (16 May 2011)
# Masked until reverse dependencies of dev-python/pyxml are updated.
=dev-lang/python-2.7.1-r2

# Dirkjan Ochtman <djc@gentoo.org> (16 May 2011)
# Mask new version for testing.
>=dev-lang/spidermonkey-2.0

# Hans de Graaff <graaff@gentoo.org> (15 May 2011)
# Mask new versions until thread-related crashes have been fixed.
# See bug 367423.
=dev-lang/ruby-enterprise-1.8.7.2011.03

# Nathan Phillip Brink <binki@gentoo.org> (13 May 2011)
#
# Abandoned by upstream. QA issues, such as failing _FORTIFY_SOURCE
# checks. Bugs 240918, 252000, 332111, 339545, 354493.
#
# Removal on 2011-07-12.
net-irc/oer
net-irc/oer-mysql

# Justin Bronder <jsbronder@gentoo.org> (09 May 2011)
#
# Out of tree kernel module and upstream does not closely
# track kernel release (current 2.8.2 only supports up to
# 2.6.32).  There are alternatives already accepted in
# the kernel such as Ceph or OCFS2. (#337374)
#
# Removal on 2011-07-09
sys-cluster/pvfs2

# Diego E. Pettenò <flameeyes@gentoo.org> (06 May 2011)
#  on behalf of QA team
#
# Fails to build with opensc-0.12.0 as it doesn't use
# PKCS#11 (bug #363019). Code looks stale, and the project
# implemented separate packages for other functions.
#
# Removal on 2011-07-05
app-crypt/scsign

# Ole Markus With <olemarkus@gentoo.org> (1 May 2011)
# Masking SVN snaphots and release candidates.
=dev-lang/php-5.3.7_rc1
=dev-lang/php-5.3.7_rc2
=dev-lang/php-5.3.7_rc3
>=dev-lang/php-5.4.0_alpha1

# Nirbheek Chauhan <nirbheek@gentoo.org> (30 Apr 2011)
# GTK3 version of notification-daemon, added for testing
# Will be unmasked once we're sure it works well with everything
>=x11-misc/notification-daemon-0.7

# Kacper Kowalik <xarthisius@gentoo.org> (29 Apr 2011)
# Fails to build with gcc 4.5. It doesn't work with latest cgal
# Bugs 320543, 344723
# Removal in 30 days
dev-python/cgal-python

# Ryan Hill <dirtyepic@gentoo.org> (23 Apr 2011)
# Masked for removal on 20110523.
app-text/gnochm

# Peter Volkov <pva@gentoo.org> (20 Apr 2011)
# Mask beta version
=net-analyzer/httping-1.5.1*

# Alexis Ballier <aballier@gentoo.org> (20 Apr 2011)
# Breaks net-voip/sflphone, non maintainer update, but is needed by
# media-video/ffmpeg
>=media-libs/celt-0.10

# Jeroen Roovers <jer@gentoo.org> (19 Apr 2011)
# development branch of argus/-clients
=net-analyzer/argus-3.0.5*
=net-analyzer/argus-clients-3.0.5*

# Luca Barbato <lu_zero@gentoo.org> (19 Apr 2011)
# Masked for testing, breaks reverse dependencies 
=media-video/libav-0.7

# Daniel Gryniewicz <dang@gentoo.org> (13 Apr 2011)
# Masked for removal in 30 days. Functionality is merged into and maintained in
# the upstream kernel.  Use any kernel (e.g. gentoo-sources) instead.
sys-kernel/usermode-sources

# Tiziano Müller <dev-zero@gentoo.org> (13 Apr 2011)
# Masked for removal in 30 days. Upstream is unresponsive
# and it does not build with recent versions of libvirt
# Use dev-php5/libvirt-php instead.
dev-php5/php-libvirt

# Patrick Lauer <patrick@gentoo.org> (12 Apr 2011)
# lacks most features and sanity, waiting for upstream to improve it
>=sys-power/powertop-1.90

# Nirbheek Chauhan <nirbheek@gentoo.org> (12 Apr 2011)
# GNOME 3 mask, things will be added here slowly
>=x11-wm/metacity-2.34
>=media-gfx/gthumb-2.13
>=gnome-extra/nautilus-open-terminal-0.19

# Diego E. Pettenò <flameeyes@gentoo.org> (8 Apr 2011)

# Merged back into the main ekeyd package as it's suggested to be used
# for heavy-loaded systems.
#
# If you wish to have only the ekey-egd-linux service, build
# app-crypt/ekeyd with USE=minimal.
#
# Removal on 2011-05-08
app-crypt/ekey-egd-linux

# Marijn Schouten <hkBst@gentoo.org> (07 April 2011)
# Masked for number of issues, but can be used to
# test against if people are impatient ;P
# Known issues:
# - Broken emacs support (ulm has promised to look)
# - doesn't build when boehm-gc is built without threads
# - no SLOTting yet!
=dev-scheme/guile-2.0.0

# Jory A. Pratt <anarchy@gentoo.org> (04 Apr 2011)
# Masked for testing.
>=mail-client/thunderbird-3.3_alpha3

# Ryan Hill <dirtyepic@gentoo.org> (02 Apr 2011)
# Masked for testing
>=sys-devel/gcc-4.6.0

# Diego E. Pettenò <flameeyes@gentoo.org> (28 Mar 2011)
#  on behalf of QA team
#
# Fails to build with recent libX11 (bug #351001); homepage
# and src_uri are gone.
#
# Removal on 2011-05-27
x11-plugins/wmnetmon

# Diego E. Pettenò <flameeyes@gentoo.org> (28 Mar 2011)
#  on behalf of QA team
#
# Doesn't respect LDFLAGS (bug #336954). Build log can easily
# show a number of other issues: doesn't respect cc, doesn't
# respect _FORTIFY_SOURCE (since headers are not properly
# included), and trying to fix it shows that the "as-is"
# license is not properly applied. The software was first
# published in 1997, then released in 2001 as a package,
# but has seen no development since.
#
# As a replacement, rather use zsh's zmv module.
#
# Removal on 2011-05-27
sys-apps/ren

# Samuli Suominen <ssuominen@gentoo.org> (28 Mar 2011)
# Masked temporarily until bug 360849 is solved so people
# can emerge udev.
=sys-apps/pciutils-3.1.7-r1

# Arfrever Frehtes Taifersar Arahesis <arfrever@gentoo.org> (25 Mar 2011)
# Some packages fail to build or work with GnuTLS built with Nettle crypto backend.
=net-libs/gnutls-2.12*

# Aaron W. Swenson <titanofold@gentoo.org> (20 Mar 2011)
# Masking 9.1 pre-release versions
>=dev-db/postgresql-docs-9.1_alpha4
>=dev-db/postgresql-base-9.1_alpha4
>=dev-db/postgresql-server-9.1_alpha4

# Stefaan De Roeck <stefaan@gentoo.org> (19 Mar 2011)
# New major upstream release.
# Needs testing before going to ~arch.
>=net-fs/openafs-1.6
>=net-fs/openafs-kernel-1.6

# Markos Chandras <hwoarang@gentoo.org> (17 Mar 2011)
# Masking openocd prelease snapshot
=dev-embedded/openocd-0.5.0_pre*

# Christian Faulhammer <fauli@gentoo.org> (12 Mar 2011)
# Mask for testing
>=www-apps/joomla-1.6.0

# Alex Alexander <wired@gentoo.org> (11 Mar 2011)
# not needed anymore. the cairo[qt4] blocker issue was fixed and
# the style is once again provided by qt-gui[gtk]
x11-themes/qgtkstyle

# Diego E. Pettenò <flameeyes@gentoo.org> (01 Mar 2011)
# Messed up build system, automagic dependencies.
dev-util/perf

# Ulrich Mueller <ulm@gentoo.org> (01 Mar 2011)
# Christian Faulhammer <fauli@gentoo.org>
# Emacs live ebuilds. Use at your own risk.
~app-editors/emacs-vcs-23.3.9999
~app-editors/emacs-vcs-24.0.9999

# Gilles Dartiguelongue <eva@gentoo.org> (23 Feb 2011)
# Masked for removal due to libgnome ABI breakage
# See bug #348644.
=games-arcade/monster-masher-1.8.1
=games-puzzle/gtetrinet-0.7.11

# Christian Faulhammer <fauli@gentoo.org> (12 Feb 2011)
# dev-vcs/bzr-gtk, dev-vcs/bzr-rewrite and dev-vcs/bzr-svn are not
# compatible with new version, upstream is notified.
>=dev-vcs/bzr-2.3.0
>=dev-vcs/bzrtools-2.3.0
~dev-vcs/qbzr-0.20.0
~dev-vcs/bzr-gtk-0.100.0
~dev-vcs/bzr-rewrite-0.6.2
~dev-vcs/bzr-svn-1.1.0

# Ryan Hill <dirtyepic@gentoo.org> (30 Mar 2011)
# Masked indefinitely (until 0.40 is released).
# http://bugs.gentoo.org/354423
>=app-pda/libopensync-0.30
>=app-pda/libopensync-plugin-file-0.30
>=app-pda/libopensync-plugin-google-calendar-0.30
>=app-pda/libopensync-plugin-irmc-0.30
>=app-pda/libopensync-plugin-palm-0.30
>=app-pda/libopensync-plugin-python-0.30
app-pda/libopensync-plugin-syncml
app-pda/libopensync-plugin-vformat
app-pda/osynctool

# Ryan Hill <dirtyepic@gentoo.org> (30 Mar 2011)
# Work in progress
# http://bugs.gentoo.org/show_bug.cgi?id=354423
app-pda/libopensync-plugin-evolution2
app-pda/libopensync-plugin-gnokii
app-pda/libopensync-plugin-gpe
app-pda/multisync-gui

# MATSUU Takuto <matsuu@gentoo.org> (30 Jan 2011)
# mask for security bug #352569
>=net-dns/maradns-2

# Pacho Ramos <pacho@gentoo.org> (26 Jan 2011)
# Doesn't work with mono-2.8, upstream tarballs for 2.99.x
# are incomplete, if you want to help on bumping, please
# refer to bug #340375
www-plugins/moonlight

# Doug Goldstein <cardoe@gentoo.org> (24 Jan 2011)
# Masked beta versions
=x11-drivers/nvidia-drivers-270.18

# Ryan Hill <dirtyepic@gentoo.org> (22 Jan 2011)
# Mask development versions due to unstable API
# as requested by leio
>=dev-python/wxpython-2.9
>=x11-libs/wxGTK-2.9

# Michael Sterrett <mr_bones_@gentoo.org> (20 Jan 2010)
# testing mask for upcoming exult release
>=games-engines/exult-1.3

# Torsten Veller <tove@gentoo.org> (06 Jan 2011)
# Next step to remove old perl and libperl versions.
# Versions prior 5.12 are masked and will be removed when 5.14 is available.
# If you are a sparc-fbsd user and your only keyworded perl version was masked,
# test perl-5.12.2 and reply to bug 288028
# For other complaints go to bug 350785
<dev-lang/perl-5.12.2
<sys-devel/libperl-5.10.1

# Sergei Trofimovich <slyfox@gentoo.org> (18 Dec 2010)
# Uses custom buildsystem. As a result has a lot of QA
# issues (like bug #282909). Known to be broken on ghc-6.10+.
# Upstream it not dead, but package needs cabal adoption
# to be maintainable.
dev-haskell/hsshellscript

# Tiziano Müller <dev-zero@gentoo.org> (13 Dec 2010)
# Mask for testing the passenger-3.0.1 module
=www-servers/nginx-0.8.53-r1

# Gilles Dartiguelongue <eva@gentoo.org> (07 Dec 2010)
# Part of GNOME 2.32 release set by breaks my machine as hell
# Needs more testing before unleashing
>=gnome-base/libbonobo-2.32

# Michal Januszewski <spock@gentoo.org> <1 Dec 2010)
# Beta NVIDIA drivers.  This is the first 260.x release that works on GF330M,
# so it might be useful for some users.
=x11-drivers/nvidia-drivers-260.19.26

# Alex Legler <a3li@gentoo.org> (28 Nov 2010)
# Not maintained, multiple security issues.
# Use the split horde ebuilds instaed.
www-apps/horde-webmail
www-apps/horde-groupware

# Sebastian Pipping <sping@gentoo.org> (27 Nov 2010)
# Test suite gives one error, previous version did not.
# Upcoming bump seems too risky for ~arch right now.
~app-text/xmlstarlet-1.0.3

# Alexis Ballier <aballier@gentoo.org> (13 Nov 2010)
# Last rites: app-text/ptex
# Ebuilds have been unmaintained for years. ptex is now merged in TeX Live with
# the 2010 version: enable the cjk useflag. If you have any feature ptex had
# that is not available with TeX Live, please file a bug.
# Will be removed on max{13 Dec 2010, TeX Live 2010 stabilized}.
# Bugs: 303965, 323559 and 344585
app-text/ptex

# Markos Chandras <hwoarang@gentoo.org> (01 Nov 2010)
# Masking alpha releases
net-analyzer/ncrack

# Peter Volkov <pva@gentoo.org> (29 Oct 2010)
# mask beta release
=net-analyzer/tcpreplay-3.4.5*

# Michael Sterrett <mr_bones_@gentoo.org> (28 Oct 2010)
# testing mask for upcoming nasm releases
>=dev-lang/nasm-2.09.999

# Markos Chandras <hwoarang@gentoo.org> (26 Oct 2010)
# master branch is heavily broken at the moment. Use
# 2.0.9999 instead if you want a kmess
# that actually builds
=net-im/kmess-9999

# Jeroen Roovers <jer@gentoo.org> (25 Oct 2010)
# Masked until reverse dependencies are sorted out (bug #342461)
>dev-libs/libnl-2

# Samuli Suominen <ssuominen@gentoo.org> (16 Oct 2010)
# dev-libs/soprano with USE redland is unported to raptor2 API
# see bug 341237
>=dev-libs/rasqal-0.9.20
>=dev-libs/redland-1.0.12

# Robin H. Johnson <robbat2@gentoo.org> (08 Oct 2010)
# Masked for testing, and some testsuite failures.
>=sys-libs/db-5.1

# Luca Barbato <lu_zero@gentoo.org> (01 Oct 2010)
# beta version, has known issues
=x11-drivers/ati-drivers-8.780*

# Arfrever Frehtes Taifersar Arahesis <arfrever@gentoo.org> (13 Sep 2010)
# Unmaintained. Masked so that Zope 2.12/2.13 is preferred.
=net-zope/zope-3*

# Doug Goldstein <cardoe@gentoo.org> (8 Sep 2010)
# masked live version
=x11-libs/cairo-9999*

# Sebastian Pipping <sping@gentoo.org> (1 Sep 2010)
# Mask upcoming bumps, potentially too hot to push to everyone
=media-libs/libdvdread-4.1.3_p1217
=media-video/dvdbackup-0.4.1

# Doug Goldstein <cardoe@gentoo.org> (28 Aug 2010)
# Need to investigate build errors on some kernels
=app-emulation/kvm-kmod-2.6.35

# Tiziano Müller <dev-zero@gentoo.org> (11 Aug 2010)
# reasons for masking:
# celt-0.8.1: not my responsability, just bumped it while doing 0.5.1.3
# opennebula: beta, testing
# qemu-kvm-spice: is going away as soon as spice patches are in qemu
app-emulation/opennebula
app-emulation/qemu-kvm-spice
=media-libs/celt-0.8.1

# Arfrever Frehtes Taifersar Arahesis <arfrever@gentoo.org> (22 Jul 2010)
# Development versions.
=net-libs/gnutls-2.11*

# Luca Barbato <lu_zero@gentoo.org> (21 Jul 2010)
# Not ready for public consumption, clashes with current mesa-git
media-libs/shivavg

# Stefan Briesenick <sbriesen@gentoo.org> (21 Jul 2010)
# doesn't compile against latest media-libs/spandsp.
# not needed anymore for Asterisk 1.6.
net-misc/asterisk-spandsp_codec_g726

# Jeroen Roovers <jer@gentoo.org> (13 Jul 2010)
# The ebuild still needs work, but if you can help testing
# it, then please unmask (bug #317325)
=net-analyzer/net-snmp-5.5*
=net-analyzer/net-snmp-5.6*

# Doug Goldstein <cardoe@gentoo.org> (07 Jul 2010)
# No actual Gentoo support yet. If you're interested, please see bug #295993
net-misc/netcf

# Markos Chandras <hwoarang@gentoo.org> (06 Jul 2010)
# Masking development releases
=x11-misc/treeline-1.3.3

# Nirbheek Chauhan <nirbheek@gentoo.org> (29 Jun 2010)
# Mask dev-libs/seed; several ebuild issues, and missing pure-runtime deps
# Also needs USE=introspection to be unmasked
dev-libs/seed

# Justin Lecher <jlec@gentoo.org> (16 Jun 10)
# Just working with the masked experimental version of refmac
>=sci-libs/monomer-db-5.16

# Robin H. Johnson <robbat2@gentoo.org> (09 Jun 2010)
# Fails to self-verify/sign in FIPS mode, pending upstream response before
# usage for GSoC project.
app-crypt/hmaccalc

# Luca Barbato <lu_zero@gentoo.org> (21 May 2010)
# development release
=dev-db/mongodb-1.5*

# Justin Lecher <jlec@gentoo.org> (16 May 2010)
# Upstreams testing version
# Added on request
>=sci-chemistry/refmac-5.6

# Robin H. Johnson <robbat2@gentoo.org> (11 May 2010)
# Masked for testing, and some testsuite failures.
# Bug 313769
=sys-libs/db-5.0*

# Tomáš Chvátal <scarabeus@gentoo.org> (27 Apr 2010)
# Replaced by >sci-libs/netcdf-4.0
sci-libs/libnc-dap

# Michael Sterrett <mr_bones_@gentoo.org> (14 Apr 2010)
# gtk+-1 is old and nasty.
# Start transitioning away from these packages.
# Read man 5 portage about package.unmask to unmask the package locally.
games-arcade/cervi
games-emulation/darcnes
games-emulation/epsxe
games-emulation/gtuxnes
games-emulation/infones
games-emulation/pcsx
games-emulation/pcsx2
games-emulation/ps2emu-cddvdlinuz
games-emulation/ps2emu-cdvdiso
games-emulation/ps2emu-dev9null
games-emulation/ps2emu-gssoft
games-emulation/ps2emu-padxwin
games-emulation/ps2emu-spu2null
games-emulation/ps2emu-usbnull
games-emulation/psemu-cdriso
games-emulation/psemu-padjoy
games-emulation/psemu-padxwin
games-emulation/psemu-peopssoftgpu
games-emulation/psemu-peopsspu
games-fps/wmquake
games-kids/gtans
games-mud/gMOO
games-puzzle/codebreaker
games-puzzle/glickomania
games-puzzle/xpuyopuyo
games-simulation/corewars
games-strategy/xscorch

# Pacho Ramos <pacho@gentoo.org> (10 Apr 2010)
# This version provides packages from testing to let people using
# latest Xorg (from ~arch or overlay) have 3D support. If you don't need it,
# keep with unmasked versions.
#
# Please don't ask for including testing packages in other emul
# packages, this is an exception to current policy because some people
# from X11 team needed 3D support even with bleeding edge Xorg.
=app-emulation/emul-linux-x86-opengl-20110129-r99

# Patrick Lauer <patrick@gentoo.org> (07 Apr 2010)
# Migrating back to unsplit samba ebuilds. Keeping samba-4 masked until release.
net-fs/samba-libs
net-fs/samba-server
net-fs/samba-client
>net-fs/samba-4

# Mounir Lamouri <volkmar@gentoo.org> (24 Mar 2010)
# Masked because breaking backward compatibility and still not needed.
>dev-util/bam-0.2.0

# Alistair Bush <ali_bush@gentoo.org> (22 Mar 2010)
# Masked due to producing build failures in stable
# lucene and possibly others. see #309961
=dev-java/javacc-5.0

# Mike Frysinger <vapier@gentoo.org> (07 Mar 2010)
# Very old packages that people should have upgraded away from
# long ago.  Courtesy mask ... time to upgrade.
# Added <sys-fs/e2fsprogs as well (halcy0n)
<sys-libs/e2fsprogs-libs-1.41.8
<sys-fs/e2fsprogs-1.41.9

# Robert Piasek <dagger@gentoo.org> (23 Feb 2010)
# Masking libmapi as it depends on masked samba4
=net-libs/libmapi-0.9

# Matthias Schwarzott <zzam@gentoo.org> (22 Jan 2010)
# since long time included in media-tv/gentoo-vdr-scripts
media-tv/vdr-dvd-scripts
media-tv/vdrplugin-rebuild

# Christian Parpart <trapni@gentoo.org> (23 Dec 2009)
# Masked for testing/feedback.
media-sound/teamspeak-server-bin

# Joerg Bornkessel <hd_brummy@gentoo.org> (02 Dec 2009)
# cvs ebuild vdr-xineliboutput-9999 masked for lifetime
=media-plugins/vdr-xineliboutput-9999

# Doug Goldstein <cardoe@gentoo.org> (07 Nov 2009)
# upstream MythTV development versions
# complain upstream if something is broken
# or if it's specific to the ebuild, any bugs that get
# filed require a patch. i.e. if you can't patch it
# you shouldn't be running this version
>=media-tv/mythtv-0.24_alpha1
>=x11-themes/mythtv-themes-0.24_alpha1
>=x11-themes/mythtv-themes-extra-0.24_alpha1
>=media-plugins/mytharchive-0.24_alpha1
>=media-plugins/mythbrowser-0.24_alpha1
>=media-plugins/mythcontrols-0.24_alpha1
>=media-plugins/mythgallery-0.24_alpha1
>=media-plugins/mythgame-0.24_alpha1
>=media-plugins/mythmovies-0.24_alpha1
>=media-plugins/mythmusic-0.24_alpha1
>=media-plugins/mythnetvision-0.24_alpha1
>=media-plugins/mythnews-0.24_alpha1
>=media-plugins/mythphone-0.24_alpha1
>=media-plugins/mythvideo-0.24_alpha1
>=media-plugins/mythweather-0.24_alpha1
>=www-apps/mythweb-0.24_alpha1
>=media-plugins/mythzoneminder-0.24_alpha1

# Peter Alfredsen <loki_val@gentoo.org> (21 Oct 2009)
# Masked because this needs a patch to be applied to portage
# to not install the kitchensink and everything else
# into /usr/src/debug with FEATURES=installsources
=dev-util/debugedit-4.4.6-r2

# Diego E. Pettenò <flameeyes@gmail.com> (9 Oct 2009)
# Untested yet; documented only in Russian, help is appreciated.
sys-auth/pam_keystore

# Peter Alfredsen <loki_val@gentoo.org> (09 Sep 2009)
# Fails to build with newest mono, causing dependency
# conflicts (#259700).
dev-lang/nemerle

# Diego E. Pettenò <flameeyes@gentoo.org> (08 Aug 2009)
#  on behalf of QA Team
#
# Mass-masking of live ebuilds; we cannot guarantee working state of
# live ebuilds, nor the availability of the server hosting them. As
# per QA team policy, all these need to be kept masked by default, if
# available in the tree.
media-tv/v4l-dvb-hg
net-irc/irssi-svn
~app-i18n/skk-jisyo-9999
net-misc/netcomics-cvs
app-portage/layman-dbtools

# Federico Ferri <mescalinum@gentoo.org> (08 Aug 2009)
# Doesn't build with Tcl 8.5, has several bugs open
=dev-tcltk/tcl-debug-2.0

# Steve Dibb <beandog@gentoo.org> (31 Jul 2009)
# Unsupported, but popular.  No plans for removal.
media-sound/alsa-driver

# Marijn Schouten <hkBst@gentoo.org> (29 Jul 2009)
# Masked for increasingly many problems. Upstream is flaky and hasn't released since 2005.
# Maxima is the only consumer and can be built with sbcl or clisp.
# Hopefully upstream will do a release that we can add to revive this package.
dev-lisp/gcl

# Jeremy Olexa <darkside@gentoo.org> (28 Jul 2009)
# On behalf of Robin H. Johnson <robbat2@gentoo.org>.
# These versions are vulnerable to GLSA's and should not be used. They will stay
# in the tree because they are useful to tracking down bugs. You have been
# warned. bug 271686
<dev-db/mysql-5.0.60-r1
<virtual/mysql-5.0

# Ben de Groot <yngwin@gentoo.org> (25 Jun 2009)
# Mask the Qt4 meta ebuild, to prevent devs from being silly and depend on
# the meta ebuild instead of on the specific split Qt ebuilds needed. See
# bug 217161 comment 11. Users may unmask this if they want to pull in all
# Qt modules, but packages in portage (or overlays) will pull in the split
# modules they need as dependency. Unmasking this will most likely pull in
# more than you need. This meta ebuild will be removed when we can add sets
# to the portage tree.
=x11-libs/qt-4*

# Nirbheek Chauhan <nirbheek@gentoo.org> (10 Jun 2009)
# Several feature regressions that will make our users
# go on a witchhunt if unmasked
# * No XDMCP connection UI
# * No configuration/theming support
# * No support for auth backends other than PAM
# * Many more...
>=gnome-base/gdm-2.26

# Benedikt Böhm <hollow@gentoo.org> (19 Apr 2009)
# masked for testing
=net-analyzer/centreon-1.4.2.7

# Tiziano Müller <dev-zero@gentoo.org> (08 Apr 2009)
# pre-releases
net-libs/libinfinity
>=app-editors/gobby-0.4.91

# Paul Varner <fuzzyray@gentoo.org> (06 Apr 2009)
# Dead upstream and has issues with newer portages.
# Due to popular demand and no suitable replacement, it will stay in the tree
# in a masked status until it completely breaks or is replaced.
app-portage/udept

# Alex Legler <a3li@gentoo.org> (20 Mar 2009)
# Ruby 1.9 for preliminary testing of libraries depending on it, bug 203706.
# Expect (some) breakages and incompatibilities.
# Want to help testing? #gentoo-ruby on Freenode
>=dev-lang/ruby-1.9.1
~virtual/ruby-ssl-1
~virtual/ruby-rdoc-1
~virtual/ruby-threads-1

# Matti Bickel <mabi@gentoo.org> (1 Mar 2009)
# Mask testing branch
=x11-libs/fox-1.7*

# Diego E. Pettenò <flameeyes@gentoo.org> (03 Jan 2009)
# These packages are not supposed to be merged directly, instead
# please use sys-devel/crossdev to install them.
dev-libs/cygwin
dev-util/mingw-runtime
dev-util/mingw64-runtime
dev-util/w32api
sys-libs/newlib

# Mike Pagano <mpagano@gentoo.org> (17 Dec 2008)
# Masked waiting on >=portage-2.2* to be unmasked
>=app-portage/portpeek-2

# Robin H. Johnson <robbat2@gentoo.org> (06 Dec 2008)
# The init.d scripts and default rules need updating and serious testing.
# Do not use the old ones with the new versions of audit, you will get weird
# crashes.
>sys-process/audit-1.7.4

# Peter Volkov <pva@gentoo.org> (16 Nov 2008)
# The development branch, to be unmasked when out of beta by upstream.
=net-misc/socat-2.0.0*

# Steve Dibb <beandog@gentoo.org> (5 Nov 2008)
# Mask realplayer codecs for security bug 245662
# http://forums.gentoo.org/viewtopic-t-713051.html
media-libs/amd64codecs
media-libs/realcodecs

# Doug Goldstein <cardoe@gentoo.org> (17 Sep 2008)
# under development
gnome-extra/gnome-lirc-properties

# Markus Ullmann <jokey@gentoo.org> (7 Jul 2008)
# mask for security bug #190667
net-irc/bitchx

# Vlastimil Babka <caster@gentoo.org> (20 May 2008)
# Masked for testing
=app-arch/rpm-5*

# Rémi Cardona <remi@gentoo.org> (16 Apr 2008)
# Masked until somebody picks it up
dev-cpp/bakery

# Chris Gianelloni <wolf31o2@gentoo.org> (3 Mar 2008)
# Masking due to security bug #194607 and security bug #204067
games-fps/doom3
games-fps/doom3-cdoom
games-fps/doom3-chextrek
games-fps/doom3-data
games-fps/doom3-demo
games-fps/doom3-ducttape
games-fps/doom3-eventhorizon
games-fps/doom3-hellcampaign
games-fps/doom3-inhell
games-fps/doom3-lms
games-fps/doom3-mitm
games-fps/doom3-phantasm
games-fps/doom3-roe
games-fps/quake4-bin
games-fps/quake4-data
games-fps/quake4-demo

# Alon Bar-Lev <alonbl@gentoo.org> (23 Feb 2008)
# These are not yet stable.
sys-fs/redirfs

# Sebastien Fabbro <bicatali@gentoo.org> (05 Feb 2008)
# sci-libs/metis-5.* still experimental
>=sci-libs/metis-4.99

# Raúl Porcel <armin76@gentoo.org> (12 Dec 2007)
# Segfaults with IMAP
=x11-plugins/replytolist-0.3.0

# Piotr Jaroszyński <peper@gentoo.org> (26 Nov 2007)
# opensync live ebuilds
=app-pda/libsyncml-9999
=app-pda/libopensync-9999
=app-pda/osynctool-9999
=app-pda/libopensync-plugin-evolution2-9999
=app-pda/libopensync-plugin-file-9999
=app-pda/libopensync-plugin-gnokii-9999
=app-pda/libopensync-plugin-google-calendar-9999
=app-pda/libopensync-plugin-gpe-9999
=app-pda/libopensync-plugin-irmc-9999
=app-pda/libopensync-plugin-palm-9999
=app-pda/libopensync-plugin-python-9999
=app-pda/libopensync-plugin-syncml-9999
=app-pda/libopensync-plugin-vformat-9999

# MATSUU Takuto <matsuu@gentoo.org> (5 Apr 2007)
# to be tested, seems unstable
>=app-i18n/scim-anthy-1.3.0

# Stefaan De Roeck <stefaan@gentoo.org> (09 Sep 2006)
# 1.5.x is a development branch, people should test 1.4.x by default
=net-fs/openafs-1.5*
=net-fs/openafs-kernel-1.5*

# Tavis Ormandy <taviso@gentoo.org> (21 Mar 2006)
# masked pending unresolved security issues #127319
games-roguelike/falconseye

# Tavis Ormandy <taviso@gentoo.org> (21 Mar 2006)
# masked pending unresolved security issues #127167
games-roguelike/slashem

# Tavis Ormandy <taviso@gentoo.org> (21 Mar 2006)
# masked pending unresolved security issues #125902
games-roguelike/nethack
games-util/hearse
games-roguelike/noegnud-nethack

# Robin H. Johnson <robbat2@gentoo.org> (11 Mar 2006)
# Work-in-progress to clean this up
# TODO
# - properly fix lazy bindings
# - fix read-only stuff
# - seperate data files from binaries
# - fix crappy state of runnable only in source tree.
# - provide log output to /var/log somewhere intelligently
app-benchmarks/ltp

# Robin H. Johnson <robbat2@gentoo.org> (11 Feb 2006)
# zlib interaction is badly broken. See bug #124733.
=dev-vcs/cvs-1.12.13*

# <klieber@gentoo.org> (01 Apr 2004)
# The following packages contain a remotely-exploitable
# security vulnerability and have been hard masked accordingly.
#
# Please see http://bugs.gentoo.org/show_bug.cgi?id=44351 for more info
#
games-fps/unreal-tournament-goty
games-fps/unreal-tournament-strikeforce
games-fps/unreal-tournament-bonuspacks
games-fps/aaut
