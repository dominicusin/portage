# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/kde-base/renamedlg-plugins/renamedlg-plugins-4.6.2.ebuild,v 1.4 2011/06/01 19:24:37 ranger Exp $

EAPI=3

KMNAME="kdebase-runtime"
KMMODULE="renamedlgplugins"
inherit kde4-meta

DESCRIPTION="KDE RenameDlg plugins"
KEYWORDS="amd64 ~arm ppc ~ppc64 x86 ~amd64-linux ~x86-linux"
IUSE="debug"
