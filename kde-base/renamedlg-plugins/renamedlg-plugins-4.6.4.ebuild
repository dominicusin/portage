# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/kde-base/renamedlg-plugins/renamedlg-plugins-4.6.4.ebuild,v 1.1 2011/06/10 17:59:49 dilfridge Exp $

EAPI=4

KMNAME="kdebase-runtime"
KMMODULE="renamedlgplugins"
inherit kde4-meta

DESCRIPTION="KDE RenameDlg plugins"
KEYWORDS="~amd64 ~arm ~ppc ~ppc64 ~x86 ~amd64-linux ~x86-linux"
IUSE="debug"
