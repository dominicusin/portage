# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/kde-base/amor/amor-4.6.2.ebuild,v 1.4 2011/06/01 17:33:27 ranger Exp $

EAPI=3

KDE_HANDBOOK="optional"
KMNAME="kdetoys"
inherit kde4-meta

DESCRIPTION="KDE: Amusing Misuse Of Resources - desktop-dwelling creature"
KEYWORDS="amd64 ppc ~ppc64 x86 ~amd64-linux ~x86-linux"
IUSE="debug"
