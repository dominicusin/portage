# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/kde-base/kdeartwork-sounds/kdeartwork-sounds-4.6.2.ebuild,v 1.4 2011/06/01 17:59:28 ranger Exp $

EAPI=3

RESTRICT="binchecks strip"

KMMODULE="sounds"
KMNAME="kdeartwork"
inherit kde4-meta

DESCRIPTION="Extra sound themes for kde"
KEYWORDS="amd64 ppc ~ppc64 x86 ~amd64-linux ~x86-linux"
IUSE=""
