# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/kde-base/kdeartwork-sounds/kdeartwork-sounds-4.6.4.ebuild,v 1.1 2011/06/10 17:59:52 dilfridge Exp $

EAPI=4

RESTRICT="binchecks strip"

KMMODULE="sounds"
KMNAME="kdeartwork"
inherit kde4-meta

DESCRIPTION="Extra sound themes for kde"
KEYWORDS="~amd64 ~ppc ~ppc64 ~x86 ~amd64-linux ~x86-linux"
IUSE=""
