# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/kde-base/ktron/ktron-4.6.4.ebuild,v 1.1 2011/06/10 18:00:18 dilfridge Exp $

EAPI=4

KMNAME="kdegames"
inherit kde4-meta

DESCRIPTION="KDE Tron game"
KEYWORDS="~amd64 ~ppc ~ppc64 ~x86 ~amd64-linux ~x86-linux"
IUSE="debug"
