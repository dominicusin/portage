# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/kde-base/qguiplatformplugin_kde/qguiplatformplugin_kde-4.6.4.ebuild,v 1.1 2011/06/10 18:00:18 dilfridge Exp $

EAPI=4

KMNAME="kdebase-workspace"
inherit kde4-meta

DESCRIPTION="Helps integration of pure Qt applications with KDE Workspace"
KEYWORDS="~amd64 ~arm ~ppc ~ppc64 ~x86 ~amd64-linux ~x86-linux"
IUSE="debug"
