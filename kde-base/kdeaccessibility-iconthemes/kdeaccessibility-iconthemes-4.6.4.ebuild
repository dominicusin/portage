# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/kde-base/kdeaccessibility-iconthemes/kdeaccessibility-iconthemes-4.6.4.ebuild,v 1.1 2011/06/10 17:59:49 dilfridge Exp $

EAPI=4

KMNAME="${PN/-*/}"
KMMODULE="IconThemes"

inherit kde4-meta

DESCRIPTION="KDE Icon Themes designed with accessibility in mind"
KEYWORDS="~amd64 ~ppc ~ppc64 ~x86 ~amd64-linux ~x86-linux"
IUSE=""
