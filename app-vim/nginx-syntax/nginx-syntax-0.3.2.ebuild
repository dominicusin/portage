# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/app-vim/nginx-syntax/nginx-syntax-0.3.2.ebuild,v 1.2 2011/03/05 00:58:48 ranger Exp $

EAPI="2"

inherit vim-plugin

DESCRIPTION="vim plugin: Nginx configuration files syntax"
HOMEPAGE="http://www.vim.org/scripts/script.php?script_id=1886"
LICENSE="as-is"
KEYWORDS="~amd64 ppc ~x86"
IUSE=""
