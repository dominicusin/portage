# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-php5/PEAR-PHP_Timer/PEAR-PHP_Timer-1.0.0.ebuild,v 1.3 2011/05/24 14:54:52 hwoarang Exp $

inherit php-pear-r1 depend.php eutils

DESCRIPTION="Utility class for timing"
LICENSE="BSD"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""
SRC_URI="http://pear.phpunit.de/get/PHP_Timer-${PV}.tgz"
